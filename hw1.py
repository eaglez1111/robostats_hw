# Eagle - Dapeng Zhao,
# dapengz@andrew or tim.eagle.zhao@gmail.com

import numpy as np
import matplotlib.pyplot as plt
import random

# For Nature type:
(stochastic, deterministic, adversarial) = (1,2,3)
# For Expert type:
(optimistic, pessimistic, oddLosing) = (1,2,3)
# For Expert type:
(WMA, RWMA) = (0,1)

class Nature:
    cnt=0
    flag=0
    def __init__(self,type=stochastic,T=100):
        self.T = T
        self.type=type
    def result(self):
        if type==stochastic:
            return random.randint(0,1)
        elif type==deterministic:   #change result every 5 matches
            cnt+=1
            if cnt>=5:
                cnt=0
                flag = flag
            return flag
        else:
            return (learner.x*learner.w) > 0.5

class Expert:
    cnt = 0
    def __init__(self,type=1):
        self.type = type
    def predict(self):
        if type==oddLosing:
            cnt+=1
            return (cnt+1)%2
        elif type==pessimistic:
            return 0
        else:
            return 1

class Learner:
    eta=0.3
    def __init__(self,type=WMA,NExpert=3):
        self.NExpert=NExpert    # Number of experts
        x = np.zeros(NExpert,dtype=int); # to save inputs from experts
        w = np.ones(NExpert,dtype=float); # to save weights for experts
        self.type=type

    def getAdvices(self):
        for i in range(NExpert):
            x[i]=expert[i].predict()

    def predict(self):
        if type==WMA:
            return self.WMA()
        else:
            return self.RWMA()

    def adjustWeights(self,result):
        for i in range(NExpert):
            w[i] -= eta*w[i]*(result!=x(i))

    def WMA(self):
        return (self.x*self.w) > 0.5

    def RWMA(self):
        choice = random.uniform(0,sum(w))
        currSum=0
        for i in range(NExpert):
            currSum+=w[i]
            if currSum>choice:
                break
        return x[i]

    def reset(self):
        w = np.ones(NExpert,dtype=float);


NNature = 3 # Number of natures
NExpert = 3 # Number of experts
T = 100

nature = [Nature(stochastic),Nature(deterministic),Nature(adversarial)]
expert = [Expert(optimistic),Expert(pessimistic),Expert(oddLosing)]
learner = Learner(WMA,NExpert)

regret = np.zeros([NExpert+1,T],dtype=float)
loss = np.zeros([NExpert+1,T],dtype=float)


for i in range(NNature):
    learner.reset();
    for t in range(T):
        learner.getAdvices();
        learner.predict










#if __name__=="__main__":
